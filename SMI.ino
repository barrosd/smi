#include <SoftwareSerial.h>   //Software porta serial
#define RxD 11
#define TxD 12

#define XPIN A1
#define YPIN A2
#define ZPIN A3

#define DEBUG_ENABLED  1

int buzzerPin = 9;  // campainha conectada no pino digital pin 9

int ledPin = 13; // LED conectado no pino digital 13

float val_x, val_y, val_z; //Armazenam valores de saída do acelerômetro.

SoftwareSerial blueToothSerial(RxD,TxD);

void setup(){
   Serial.begin(9600); //Inicializando comunicação serial com o computador
   pinMode(ledPin, OUTPUT);
//   digitalWrite(ledPin, HIGH);
   pinMode(buzzerPin, OUTPUT); 	// seta campainha como saída
   pinMode(RxD, INPUT);
   pinMode(TxD, OUTPUT);
   setupBlueToothConnection();
   Serial.print("X \t Y \t Z \n");
}

void loop(){
  if(blueToothSerial.read() == 'a')
  {
    blueToothSerial.println("Você está conectado!");
    //You can write you BT communication logic here
  }
  val_x = analogRead(XPIN); //Recebe valor do pino X do acelerômetro.
  val_y = analogRead(YPIN); //Recebe valor do pino Y do acelerômetro.
  val_z = analogRead(ZPIN); //Recebe valor do pino Z do acelerômetro.
  
  val_x = to_g(val_x, 'x');
  val_y = to_g(val_y, 'y');
  val_z = to_g(val_z, 'z');
  
  Serial.print(val_x);
  Serial.print("\t");
  Serial.print(val_y);
  Serial.print("\t");
  Serial.print(val_z);
  Serial.print("\n");
  
  delay(100);
  
  if(val_x > 1.00 && val_y < 0.00 && val_z > 0.00){
    Serial.print("A pessoa caiu de frente!!!");
    blueToothSerial.println("A pessoa caiu de frente!!!");
    scale();
  }
  else if(val_x < -1.00 && val_y < 0.00 && val_z > 0.00 && val_z < 2.00){
    Serial.print("A pessoa caiu de costa!!!");
    blueToothSerial.println("A pessoa caiu de costas!!!");
    scale(); 
  }
  else if(val_x < 0.00 && val_x > -1.00 && val_y < -1.00 && val_z > 2.00){
    Serial.print("A pessoa caiu do lado direito!!!");
    blueToothSerial.println("A pessoa caiu do lado direito!!!");
    scale(); 
  }
  else if(val_x < 0.00 && val_y < 0.00 && val_z < -1.00){
    Serial.print("A pessoa caiu do lado esquerdo!!!");
    blueToothSerial.println("A pessoa caiu do lado esquerdo!!!");
    scale();
  }
  else{
    Serial.print("Nenhum queda!!!");
    digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(1000);
    //scale();
    //digitalWrite(ledPin, LOW);
    //delay(1000);
  }
  
}

void setupBlueToothConnection()
{
    blueToothSerial.begin(38400);
    delay(1000);
    sendBlueToothCommand("\r\n+STWMOD=0\r\n");
    sendBlueToothCommand("\r\n+STNA=DetectaQueda\r\n");
    sendBlueToothCommand("\r\n+STAUTO=0\r\n");
    sendBlueToothCommand("\r\n+STOAUT=1\r\n");
    sendBlueToothCommand("\r\n +STPIN=0000\r\n");
    delay(2000); 
    sendBlueToothCommand("\r\n+INQ=1\r\n");
    delay(2000); 
}

float to_g(const int axis_value, char axis){
  float volt = (3.3 * axis_value/1023);
    switch(axis){
      case 'x':
        return (volt - 1.63)/0.33;
        break;
      case 'y':
        return (volt - 1.63)/0.33;
        break;
      case 'z':
        return (volt - 1.75)/0.33;
        break;
  }
}

void CheckOK()
{
  char a,b;
  while(1)
  {
    if(blueToothSerial.available())
    {
    a = blueToothSerial.read();
 
    if('O' == a)
    {
      while(blueToothSerial.available()) 
      {
         b = blueToothSerial.read();
         break;
      }
      if('K' == b)
      {
        break;
      }
    }
   }
  }
 
  while( (a = blueToothSerial.read()) != -1)
  {

  }
}
 
void sendBlueToothCommand(char command[])
{
    blueToothSerial.print(command);
    CheckOK();   
}

void beep(unsigned char buzzerPin, int frequencyInHertz, long timeInMilliseconds) 	//the sound producing function 
{ 	
  int x; 	 
  long delayAmount=(long)(1000000/frequencyInHertz); 	 
  long loopTime = (long)((timeInMilliseconds*1000)/(delayAmount*2)); 	
  for (x=0;x<loopTime;x++) 	 
  { 	 
    digitalWrite(buzzerPin,HIGH); 	 
    delayMicroseconds(delayAmount); 	 
    digitalWrite(buzzerPin,LOW); 	 
    delayMicroseconds(delayAmount); 	 
  } 	 
} 	 

int scale(){
  digitalWrite(ledPin,HIGH); 	//turn on the LED
  beep(buzzerPin,2093,500); 	
  beep(buzzerPin,2093,500);
  beep(buzzerPin,2093,500); 	
  beep(buzzerPin,2093,500);
  beep(buzzerPin,2093,500); 	
  beep(buzzerPin,2093,500);
  beep(buzzerPin,2093,500); 	
  beep(buzzerPin,2093,500);
  digitalWrite(ledPin,LOW); 	//turn off the LED 
}


